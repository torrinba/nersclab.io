# Science Use-Cases 

Machine Learning and Deep Learning are increasingly used to analyze
scientific data, in fields as diverse as neuroscience, climate science
and particle physics. In this page you will find links to examples of
scientific use cases using deep learning at NERSC.

![Examples of how machine learning is being used for science at NERSC](../images/science_use_case_spider.png)

## Science Use-Cases

We have assembled some examples of machine learning projects being
carried out at NERSC, in most cases including links to the
codebase. These cover a variety of science areas, and in many cases
showcase the use of Tensorflow optimized for the KNL architecture.

* [Using deep networks for HEP physics analyses](hep-cnn.md)
* [Using deep networks for neutrino telescopes (Ice Cube)](https://www.nersc.gov/news-publications/nersc-news/nersc-center-news/2018/icecube-research-garners-best-paper-award-at-ieee-machine-learning-conference/)
* [CosmoGAN: Deep networks for generating cosmology mass maps](https://www.nersc.gov/news-publications/nersc-news/science-news/2019/cosmogan-training-a-neural-network-to-study-dark-matter/)
* A use of SciKitLearn by Juliette Ugirumurera can be found in [this
  iPython
  notebook](https://github.com/NERSC/data-day-examples/blob/master/SLURM_challenge.ipynb). The
  code uses SciKitLearn to construct, train and evaluate the network,
  and was the winning code for the SLURM log data challenge in the
  [2017 Data Day
  Competition](https://www.nersc.gov/users/NUG/annual-meetings/nersc-data-day-and-nug2017/data-competition/).
* The winning code for the Astronomy challenge in the [2017 Data Day
  Competition](https://www.nersc.gov/users/NUG/annual-meetings/nersc-data-day-and-nug2017/data-competition/)
  by Yisha Sun uses TensorFlow to set up and train the network. The
  code can be found in [this github
  repository](https://github.com/miaoshasha/Astronomical_Classification).
* Many other projects from LBNL can be found at https://ml4sci.lbl.gov
