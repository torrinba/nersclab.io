# Analytics

Analytics is key to gaining insights from massive, complex datasets.
NERSC provides general purpose analytics (iPython, Spark, MATLAB, IDL,
Mathematica, ROOT), statistics (R), machine learning, and imaging
tools.

Scientific Visualization is the process of creating visual imagery
from raw scientific data. NERSC supports the VisIt and Paraview tools
for visualizing and interacting with generic scientific datasets. NCAR
Graphics Library is provided (as a specialized package) for climate
data.

The field of information visualization deals with rendering datasets
that do not necessarily map onto a natural 2D or 3D co-ordinate
system. R (ggplot2) and python (matplotlib) provide capabilities for
information visualization.

* [Dask](dask.md)
* [Spark](spark.md)
* [Paraview](../applications/paraview/index.md)
* [Visit](../applications/visit/index.md)
* [NCAR Graphics](../applications/ncargraphics/index.md)
