# Current known issues

## Perlmutter

!!! warning "Perlmutter is not a production resource"
    Perlmutter is not a production resource. While we will attempt to
    make the system available to users as much as possible, it is
    subject to performance variability, unannounced and unexpected
    outages, reconfigurations, and periods of restricted
    access. Please visit the [timeline
    page](systems/perlmutter/timeline/index.md) for more information
    about changes we've made in our recent upgrades.

NERSC has automated monitoring that tracks failed nodes, so please
only open tickets for node failures if the node consistently has poor
performance relative to other nodes in the job or if the node
repeatedly causes your jobs to fail.

### New issues

- Perlmutter's network is undergoing a period of verification and
  hardening. We expect there will be occasional problems accessing and
  listing files (usually expressed as a stale file handle error) as
  well as performance variation or network timeouts for Perlmutter
  jobs. We are developing monitoring to detect and respond to these
  issues, but since this technology is completely new, we do not yet
  know automatic ways to detect all the failure modes. We expect this
  work to extend to at least the end of November, 2022. We appreciate
  your patience while we work to improve Perlmutter's network.

- If an instance of your `scrontab` job is cancelled (for instance
  because of a maintenance or the node it's running on needed to be
  rebooted), the entire scrontab entry will go into a `DISABLED`
  state. We've opened a request with SchedMD to get this behavior
  amended to just block a single instance of the scrontab instead of
  the whole thing. For now, you will have to manually unblock your
  scrontabs (i.e. delete the `DISABLED` text in front of your scrontab
  entry).

- MPICH returns null bytes when collectively reading with MPI-IO from
  a file on `/pscratch` using progressive file layout (PFL) and
  exceeding a boundary between the different striping factors. If you
  want to use PFL, you can use `export
  MPICH_MPIIO_HINTS="*:romio_cb_read=disable"` to disable collective
  reading.

- [Host-based authentication](connect/mfa/#host-based-authentication) 
  is currently not working on Perlmutter. You can still ssh between nodes, 
  but will need your password and OTP

### Ongoing issues

- Our slingshot 11 libfabric (Perlmutter CPU nodes) is currently missing the functions
  `MPI_Mprobe()/MPI_Improbe()` and `MPI_Mrecv()/MPI_Imrecv()`. This
  may especially impact mpi4py users attempting to send/receive pickled
  objects. One workaround may be to set `export MPI4PY_RC_RECV_MPROBE='False'`.
  The current estimated timeframe for a fix is January 2023.
  If these missing functions are impacting your workload, please open a ticket
  to let us know.
- MPI users may hit `segmentation fault` errors when trying
  to launch an MPI job with many ranks due to incorrect
  allocation of GPU memory. We provide [more information
  and a suggested workaround](systems/perlmutter/index.md#known-issues-with-cuda-aware-mpi).
- You may see messages like `-bash:
  /usr/common/usg/bin/nersc_host: No such file or directory` when you
  login. This means you have outdated dotfiles that need to be
  updated. To stop this message, you can either delete this line from
  your dot files or check if `NERSC_HOST` is set before overwriting
  it. Please see our [environment
  page](environment/index.md#home-directories-shells-and-dotfiles)
  for more details.
- Shifter user may see errors about `BIND MOUNT FAILED` if they
  attempt to volume mount directories that are not world
  executable. We have [some workarounds for this
  issue](../development/shifter/issues/#invalid-volume-map).
- [Known issues for Machine Learning applications](machinelearning/known_issues.md)
- Users may notice MKL-based CPU code runs more slowly. Please try
  `module load fast-mkl-amd`.

!!! caution "Be careful with NVIDIA Unified Memory to avoid crashing nodes"
    In your code, [NVIDIA Unified Memory](https://developer.nvidia.com/blog/unified-memory-cuda-beginners/)
    might
    look something like `cudaMallocManaged`. At the moment, we do not have
    the ability to control this kind of memory and keep it under a safe
    limit. Users who allocate a large
    pool of this kind of memory may end up crashing nodes if the UVM
    memory does not leave enough room for necessary system tools like
    our filesystem client.
    We expect a fix from the vendor soon. In the meantime, please keep the size
    of memory pools allocated via UVM relatively small. If you have
    questions about this, please contact us.

### Past Issues

For updates on past issues affecting Perlmutter, see the
[timeline page](systems/perlmutter/timeline/index.md).

## Cori

### Ongoing issues

- The Burst Buffer on Cori has a number of known issues, documented at
  [Cori Burst Buffer](filesystems/cori-burst-buffer.md#known-issues): its
  usage is discouraged.
