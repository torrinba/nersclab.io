# NCAR Graphics

NCAR Graphics is a Fortran and C based software package for scientific
visualization. NCAR provides
[official documentation regarding NCAR Graphics](http://ngwww.ucar.edu).

!!! warning "NCAR Graphics is no longer supported"
    NCAR no longer supports NCAR Graphics. It has been replaced by
    [GeoCAT](https://geocat.ucar.edu/), which is a collection of Python packages
    related to the NCAR Command Language [NCL](https://github.com/NCAR/ncl), which
    also is no longer being developed.  Consequently, NCAR Graphics software
    available NERSC is provided "as is" and without support, and may be removed at
    a future date. NERSC users are strongly encouraged to migrate their workflows
    to GeoCAT.

## Usage Summary

NCL is available on Cori as the module `ncl`, which provides the NCAR Graphics
compiler wrappers `ncargf77`, `ncargf90`, and `ncargcc`. The `ncar` module
defines the environment variables `NCAR` and `NCARC` which contain the full
list of compiler switches required to compile NCAR Graphics applications.

## Related Documentation

The following is a list of related Web documentation for NCL:

   * [NCL Tools Documentation](http://www.ncl.ucar.edu/Document/Tools/)
   * [NCL Reference Manual](http://www.ncl.ucar.edu/Document/Manuals/Ref_Manual/)
   * [Getting Started using NCL](http://www.ncl.ucar.edu/Document/Manuals/Getting_Started/)
   * [Category List of NCL Applications](http://www.ncl.ucar.edu/Applications/)
